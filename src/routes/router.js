import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "../pages/Home";
import NotFound from "../pages/404";
import Profile from "../pages/Profile";
import DetailPageOverview from "../pages/DetailPageOverview";
import Overview from "../components/Overview";
import Review from "../components/Review";
import ModalHome from "../components/ModalHome.jsx";
import PageFaq from "../pages/page-faq.jsx";
import PageSettings from "../pages/page-settings.jsx";
import PageCharacter from "../pages/page-character.jsx";

export const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/overview" exact>
          <Overview />
        </Route>
        <Route path="/review" exact>
          <Review />
        </Route>
        <Route path="/profile" exact>
          <Profile />
        </Route>
        <Route path="/detailpage" exact>
          <DetailPageOverview />
        </Route>

        <Route path="/modalHome" exact>
          <ModalHome />
        </Route>

        <Route path="/pagefaq" exact>
          <PageFaq />
        </Route>

        <Route path="/page-settings" exact>
          <PageSettings />
        </Route>

        <Route path="/page-character" exact>
          <PageCharacter />
        </Route>

        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    </Router>
  );
};

export default Routes;
