import React from "react";
import pic from "../components/assets/img/not-found.gif";
import "../components/assets/styles/Not-Found.sass"

const NotFound = () => {

  return (
    <>
      <div className="not-found">
        <img className="d-block h-100 w-100" src={pic} alt="not found" fluid />
      </div>
    </>
  );
};

export default NotFound;
