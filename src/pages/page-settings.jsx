import React from "react";
import NavBar from "../components/navbar";
import Footer from "../components/footer";
import Settings from "../components/settings"

function PageSettings() {
  return (
    <div className="page-settings">
      <NavBar />
      <Settings />
      <Footer />
    </div>
  );
}

export default PageSettings;