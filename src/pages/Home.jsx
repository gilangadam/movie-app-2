import React from "react";
import NavBar from "../components/navbar";
import Carousels from "../components/carousel";
import Footer from "../components/footer";
import ButtonCategoryGenre from "../components/buttonCategoryGenre";
import Content from "../components/content";


function Home() {
  return (
    <div className="home">
      <NavBar />
      <Carousels />
      <ButtonCategoryGenre />
      <Content />
      <Footer />
    </div>
  );
}

export default Home;
