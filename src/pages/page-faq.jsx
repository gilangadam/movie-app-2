import React from "react";
import NavBar from "../components/navbar";
import Footer from "../components/footer";
import PageFaqs from "../components/faq"

function PageFaq() {
  return (
    <div className="pagefaq">
      <NavBar />
      <PageFaqs />
      <Footer />
    </div>
  );
}

export default PageFaq;