import React from "react";
import NavBar from "../components/navbar";
import Footer from "../components/footer";
import Jumbotron from "../components/Jumbotron";
import ButtonDetail from "../components/ButtonDetail";
import Overview from "../components/Overview";

function DetailPageOverview() {
  return (
    <div className="detailpage">
      <NavBar />
      <Jumbotron />
      <ButtonDetail />
      <Overview />
      <Footer />
    </div>
    
  );
}

export default DetailPageOverview;

