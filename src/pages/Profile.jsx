import React from "react";
import NavBar from "../components/navbar";
import Footer from "../components/footer";
import UserProfile from "../components/userProfile";

function Profile() {
  return (
    <div className="profile">
      <NavBar />
      <UserProfile />
      <Footer />
    </div>
  );
}

export default Profile;
