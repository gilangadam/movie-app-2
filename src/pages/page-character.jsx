import React from "react";
import NavBar from "../components/navbar";
import Footer from "../components/footer";
import Character from "../components/characters"

function PageCharacter() {
  return (
    <div className="page-characters">
      <NavBar />
      <Character />
      <Footer />
    </div>
  );
}

export default PageCharacter;