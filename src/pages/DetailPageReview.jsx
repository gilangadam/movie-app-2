import React from "react";
import NavBar from "../components/navbar";
import Footer from "../components/footer";
import Jumbotron from "../components/Jumbotron";
import ButtonDetail from "../components/ButtonDetail";
import Review from "../components/Review";

function DetailPageReview() {
  return (
    <div className="detailpage">
      <NavBar />
      <Jumbotron />
      <ButtonDetail />
      <Review />
      <Footer />
    </div>
    
  );
}

export default DetailPageReview;

