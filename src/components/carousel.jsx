import React from "react";
import { Carousel } from "react-bootstrap";
import pic1 from "./assets/img/pic1.jpg";
import pic2 from "./assets/img/pic2.jpg";
import pic3 from "./assets/img/pic3.jpg";
import "./assets/styles/Carousel.sass";

function Carousels() {
  return (
    <div className="carousel-container">
      <Carousel>
        <Carousel.Item>
          <img className="d-block w-100" src={pic1} alt="First slide" fluid />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={pic2} alt="Second slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={pic3} alt="Third slide" fluid />
        </Carousel.Item>
      </Carousel>
    </div>
  );
}

export default Carousels;
