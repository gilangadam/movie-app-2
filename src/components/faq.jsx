import React from 'react';
import { Accordion, Card} from 'react-bootstrap'
import "../components/assets/styles/faq.scss"



const Faq = () => {
    
  return (
      <div class="body-faq">
          <div class="header-body">
              <div class="container-h2">
          <h2>Frequently Asked Questions (FAQs)</h2>
          </div>
    <Accordion defaultActiveKey="0">
    <Card style={{marginLeft: "350px", marginRight: "350px"}}>
      <Accordion.Toggle as={Card.Header} eventKey="0">
        What is Team-G TV?
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="0">
        <Card.Body style={{color: "#E5E5E5", backgroundColor:"#343434"}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</Card.Body>
      </Accordion.Collapse>
    </Card>
    <Card style={{marginLeft: "350px", marginRight: "350px"}}>
      <Accordion.Toggle as={Card.Header} eventKey="1">
        What is our privacy policy?
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="1">
        <Card.Body style={{color: "#E5E5E5", backgroundColor:"#343434"}}>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Card.Body>
      </Accordion.Collapse>
    </Card>
    <Card style={{marginLeft: "350px", marginRight: "350px"}}>
      <Accordion.Toggle as={Card.Header} eventKey="2">
        What language that we provide?
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="2">
        <Card.Body style={{color: "#E5E5E5", backgroundColor:"#343434"}}>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Card.Body>
      </Accordion.Collapse>
    </Card>
    <Card style={{marginLeft: "350px", marginRight: "350px"}}>
      <Accordion.Toggle as={Card.Header} eventKey="3">
       How to add movie to watch list?
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="3">
        <Card.Body style={{color: "#E5E5E5", backgroundColor:"#343434"}}>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Card.Body>
      </Accordion.Collapse>
    </Card>
    <Card style={{marginLeft: "350px", marginRight: "350px"}}>
      <Accordion.Toggle as={Card.Header} eventKey="4">
        How to be an admin?
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="4">
        <Card.Body style={{color: "#E5E5E5", backgroundColor:"#343434"}}>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Card.Body>
      </Accordion.Collapse>
    </Card>
    <Card style={{marginLeft: "350px", marginRight: "350px"}}>
      <Accordion.Toggle as={Card.Header} eventKey="5">
        How to report a review?
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="5">
        <Card.Body style={{color: "#E5E5E5", backgroundColor:"#343434"}}>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Card.Body>
      </Accordion.Collapse>
    </Card>
    <Card style={{marginLeft: "350px", marginRight: "350px"}}>
      <Accordion.Toggle as={Card.Header} eventKey="6">
        How to manage my review?
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="6">
        <Card.Body style={{color: "#E5E5E5", backgroundColor:"#343434"}}>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Card.Body>
      </Accordion.Collapse>
    </Card>
    <Card style={{marginLeft: "350px", marginRight: "350px"}}>
      <Accordion.Toggle as={Card.Header} eventKey="7">
       How to change language?
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="7">
        <Card.Body style={{color: "#E5E5E5", backgroundColor:"#343434"}}>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Card.Body>
      </Accordion.Collapse>
    </Card>
    <Card style={{marginLeft: "350px", marginRight: "350px"}}>
      <Accordion.Toggle as={Card.Header} eventKey="8">
       How to create new profile?
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="8">
        <Card.Body style={{color: "#E5E5E5", backgroundColor:"#343434"}}>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Card.Body>
      </Accordion.Collapse>
    </Card>
  </Accordion>
  </div>
  </div>
  );
}

export default Faq;