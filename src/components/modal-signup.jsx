import React, { useState } from "react";
import "./assets/styles/signup.scss";
import { ModalBody, Modal, Button } from "reactstrap";
import ModalSignIn from "../components/modal-signin"
import { Link } from "react-router-dom";
// import 'bootstrap/dist/css/bootstrap.min.css';

const ModalSignUp = () => {
  const [modalSignIn, setModalSignIn] = useState(false);
  const toggleModalSignIn = () => setModalSignIn(!modalSignIn);
  return (
    <ModalBody>
      <div>
        <form>
          <div className="container-signup">
            <div className="header-container">
              <h1 style={{ textAlign: "center" }}>Sign Up</h1>
            </div>
            <label className="label-signup" for="full-name">
              <b>Full Name</b>
            </label>
            <input
              className="input-signup"
              type="text"
              placeholder="enter your name"
              name="full-name"
              required
            />
            <label className="label-signup" for="username">
              <b>Username</b>
            </label>
            <input
              className="input-signup"
              type="text"
              placeholder="enter username"
              name="username"
              required
            />
            <label className="label-signup" for="email">
              <b>Email</b>
            </label>
            <input
              className="input-signup"
              type="text"
              placeholder="enter your email"
              name="email"
              required
            />
            <label className="label-signup" for="password">
              <b>Password</b>
            </label>
            <input
              className="input-signup"
              type="text"
              placeholder="enter your password"
              name="password"
              required
            />
            <label className="label-signup" for="password">
              <b>Confirm Password</b>
            </label>
            <input
              className="input-signup"
              type="text"
              placeholder="confirm password"
              name="password"
              required
            />
            <br />
            <label>
              <br />
              <input
                type="checkbox"
                defaultChecked="checked"
                name="remember"
              />{" "}
              Remember me
            </label>
            <br />
            <br />
            <button type="submit" className="signup-btn">
              <b>Sign Up Now!</b>
            </button>
            <p style={{ textAlign: "center" }}>
              Already have an account?
              <button onClick={toggleModalSignIn}>
                Log In
              </button>
              <Modal isOpen={modalSignIn} toggle={toggleModalSignIn}>
                <ModalSignIn />
              </Modal>
            </p>
          </div>
        </form>
      </div>
    </ModalBody>
  );
};

export default ModalSignUp;
