import React from 'react';
import StarRatingComponent from 'react-star-rating-component';
import "./assets/styles/Jumbotron.scss";
class Jumbo extends React.Component {
    constructor() {
        super();
    
        this.state = {
          rating: 1
        };
      }
  render() {
      const { rating } = this.state;
      return (          
            <div className="scss-dived">
                <div class="jumbotron">
                    <h1 class="display-4" >SAINT SEIYA</h1>
                    <div>
                        <StarRatingComponent 
                        className="star2" 
                        name="rate2" 
                        editing={false}
                        starCount={5}
                        emptyStarColor="#E5E5E5"
                        value={rating}
                        />
                    </div>
                    <p className="ref">2022 Reviews</p>
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqu
                        </p>
                        <button type="button" class="btn btn-warning btn-lg">Watch Trailer</button>
                        <button type="button" class="btns btn-lg">Add to Watchlist</button>
                </div>
            </div>
    );
  }
}
export default Jumbo;

