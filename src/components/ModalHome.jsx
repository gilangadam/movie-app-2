import React, { useState } from "react";
import { Button, Modal, Row, Col } from "reactstrap";
import ModalSignUp from "./modal-signup"
import ModalSignIn from "./modal-signin";
import ModalEditProfile from "./modal-editprofile";

function ModalHome() {
  const [modalSignUp, setModalSignUp] = useState(false);
  const toggleModalSignUp = () => setModalSignUp(!modalSignUp);

  const [modalSignIn, setModalSignIn] = useState(false);
  const toggleModalSignIn = () => setModalSignIn(!modalSignIn);

  const [modalEditProfile, setModalEditProfile] = useState(false);
  const toggleModalEditProfile = () => setModalEditProfile(!modalEditProfile);

  return (
    <Row>
      <Col>
        <Button color="danger" onClick={toggleModalSignUp}>
          Modal Sign up
        </Button>
        <Modal isOpen={modalSignUp} toggle={toggleModalSignUp}>
          <ModalSignUp />
        </Modal>
        <Button color="danger" onClick={toggleModalSignIn}>
          Modal Sign In
        </Button>
        <Modal isOpen={modalSignIn} toggle={toggleModalSignIn}>
          <ModalSignIn />
        </Modal>
        <Button color="danger" onClick={toggleModalEditProfile}>
          Modal Edit Profile
        </Button>
        <Modal isOpen={modalEditProfile} toggle={toggleModalEditProfile}>
          <ModalEditProfile />
        </Modal>
      </Col>
    </Row>
  );
}

export default ModalHome;
