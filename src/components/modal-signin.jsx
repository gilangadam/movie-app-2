import React, { useState } from "react";
import { ModalBody, Modal, Button } from "reactstrap";
import ModalSignUp from "../components/modal-signup"
import "./assets/styles/login.scss"

const ModalSignIn = () => {

  const [modalSignUp, setModalSignUp] = useState(false);
  const toggleModalSignUp = () => setModalSignUp(!modalSignUp);

  return (
    <ModalBody>
    <div>
      <form>
        <div className="container-signin">
        <div className="header-container">
            <h1 className="header-container" style={{textAlign:"center"}}>Log In</h1>
          </div>
          <label className="label-signin" for="email">
            <b>Email</b>
          </label>
          <input
            className="input-signin"
            type="text"
            placeholder="enter your email"
            name="email"
            required
          />

          <label className="label-signin" for="password">
            <b>Password</b>
          </label>
          <input
            className="input-signin"
            type="text"
            placeholder="enter your password"
            name="password"
            required
          />
          <br></br>

          <label>
            <br></br>
            <input type="checkbox" checked="checked" name="remember" /> Remember
            me
          </label>
          <br></br>

          <button type="submit" className="signin-btn">
            <b style={{textAlign:"center"}}>Log In</b>
          </button>

          <p style={{textAlign:"center"}}>
            Don't have account?{" "}
            <Button onClick={toggleModalSignUp}>
          Sign up
        </Button>
        <Modal isOpen={modalSignUp} toggle={toggleModalSignUp}>
          <ModalSignUp />
        </Modal>
          </p>
        </div>
      </form>
    </div>
    </ModalBody>
  );
};

export default ModalSignIn;
