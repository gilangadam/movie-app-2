import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/styles/Navbar.sass";
import logo from "./assets/img/TeamG-crop.gif";
import React, { useState } from "react";
import { Button, Modal, Row, Col } from "reactstrap";
import ModalSignUp from "./modal-signup"

const NavBar = () => {
  const [modalSignUp, setModalSignUp] = useState(false);
  const toggleModalSignUp = () => setModalSignUp(!modalSignUp);

  return (
    <header>
      <div className="navbar-container w-100">
        <div className="navbar">
          <img src={logo} alt="Team G" className="navbar-logo"></img>
          <input placeholder="search movie" className="navbar-search"></input>
          <Button onClick={toggleModalSignUp}>
          Sign Up
        </Button>
          <Modal isOpen={modalSignUp} toggle={toggleModalSignUp}>
          <ModalSignUp />
        </Modal>
        </div>
      </div>
    </header>
  );
};

export default NavBar;
