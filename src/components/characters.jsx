import React from "react";
import "../components/assets/styles/page-character.scss";
import image from "../components/assets/img/profilepicture.png";
import "../components/assets/styles/bootstrap.css";

const Characters = () => {
  return (
    <body class="body">
        <div class="button-container">
      <button class="btn-character">
        <b>Overview</b>
      </button>
      <button class="btn-character">
        <b>Character</b>
      </button>
      <button class="btn-character">
        <b>Review</b>
      </button>
      </div>

    
      <div class="container">
      <div class="row">
        <div class="col">
        <div class="card" className="width: 18rem;">
          <div class="card-body">
          <img src={image} class="card-img-top" alt="..." />
            <h5 class="card-title">Card title</h5>
          </div>
        </div>
        
      </div>
      <div class="col">
        <div class="card" className="width: 18rem;">
          <div class="card-body">
          <img src={image} class="card-img-top" alt="..." />
            <h5 class="card-title">Card title</h5>
          </div>
        </div>
        
      </div>
      <div class="col">
        <div class="card" className="width: 18rem;">
          <div class="card-body">
          <img src={image} class="card-img-top" alt="..." />
            <h5 class="card-title">Card title</h5>
          </div>
        </div>
        
      </div>
      <div class="col">
        <div class="card" className="width: 18rem;">
          <div class="card-body">
          <img src={image} class="card-img-top" alt="..." />
            <h5 class="card-title">Card title</h5>
          </div>
        </div>
        
      </div>
      <div class="col">
        <div class="card" className="width: 18rem;">
          <div class="card-body">
          <img src={image} class="card-img-top" alt="..." />
            <h5 class="card-title">Card title</h5>
          </div>
        </div>
        
      </div>
      <div class="col">
        <div class="card" className="width: 18rem;">
          <div class="card-body">
          <img src={image} class="card-img-top" alt="..." />
            <h5 class="card-title">Card title</h5>
          </div>
        </div>
        </div>
      </div>
      
      </div>
      
    </body>
  );
};

export default Characters;
