import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col, ListGroup, ListGroupItem, Card, CardTitle } from 'reactstrap';
import React, {useState} from 'react';
import classnames from 'classnames';
import image from "../components/assets/img/profilepicture.png"
import { BsPersonPlus, BsFillPeopleFill, BsWrench, BsInfoCircleFill, BsFileText, BsFlag, BsXCircle } from "react-icons/bs";
import "../components/assets/styles/settings.scss"

const Settings = () => {
        const [activeTab, setActiveTab] = useState('1');
      
        const toggle = tab => {
          if(activeTab !== tab) setActiveTab(tab);}

    return (
        <div>
        <body class="body">
            <h1 style={{color: "#E5E5E5"}}>Settings</h1>
            <div>
      <Nav tabs>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '1' })}
            onClick={() => { toggle('1'); }} 
          >
            Switch Profile
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '2' })}
            onClick={() => { toggle('2'); }}
          >
            Language
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '3' })}
            onClick={() => { toggle('3'); }}
          >
            Display
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '4' })}
            onClick={() => { toggle('4'); }}
          >
           Privacy and Safety
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
          <Row>
            <Col sm="12">
            <img src={image} class="image-card" alt=""></img>
            <p style={{color: "#E5E5E5"}}> Username</p>
            <p style={{color: "#E5E5E5"}}>Add Existing Account  <BsPersonPlus /></p>
            <p style={{color: "#E5E5E5"}}>Switch Account  <BsFillPeopleFill /></p>
            </Col>
          </Row>
        </TabPane>
        <TabPane tabId="2">
          <Row>
            <Col sm="6">
              <h3 style={{color: "#E5E5E5"}}>Display Language</h3>
              <ListGroup>
      <ListGroupItem style={{color: "#E5E5E5", backgroundColor:"#343434"}}><b>English</b></ListGroupItem>
      <ListGroupItem style={{color: "#E5E5E5", backgroundColor:"#343434"}}><b>Indonesian</b></ListGroupItem>
      <ListGroupItem style={{color: "#E5E5E5", backgroundColor:"#343434"}}><b>Russian</b></ListGroupItem>
      <ListGroupItem style={{color: "#E5E5E5", backgroundColor:"#343434"}}><b>Japanese</b></ListGroupItem>
      <ListGroupItem style={{color: "#E5E5E5", backgroundColor:"#343434"}}><b>Korean</b></ListGroupItem>
    </ListGroup>
            </Col>
          </Row>
        </TabPane>
        <TabPane tabId="3">
          <Row>
            <Col sm="6">
            <Card body>
        <CardTitle tag="h5" style={{color: "#E5E5E5"}}>Font Size</CardTitle>
        <button size="sm" class="btn-size-small"><b>Small</b></button>
        <button class="btn-size-medium"><b>Normal</b></button>
        <button size="lg" class="btn-size-large"><b>Large</b></button>
      </Card>
      <Card body>
      <CardTitle tag="h5" style={{color: "#E5E5E5"}}>Background</CardTitle>
      <button class="btn-dark-mode"><input type="checkbox" /><b>Dark</b></button>
      <button class="btn-light-mode"><input type="checkbox" /><b>Light</b></button>
      </Card>
            </Col>
          </Row>
        </TabPane>
        <TabPane tabId="4">
          <Row>
            <Col sm="12">
            <ListGroup>
      <ListGroupItem style={{color: "#E5E5E5", backgroundColor:"#343434"}}><BsInfoCircleFill />  <b>Privacy Policy</b></ListGroupItem>
      <ListGroupItem style={{color: "#E5E5E5", backgroundColor:"#343434"}}><BsFileText />  <b>Privacy Center</b></ListGroupItem>
      <ListGroupItem style={{color: "#E5E5E5", backgroundColor:"#343434"}}><BsFlag />  <b>Report Review</b></ListGroupItem>
      <ListGroupItem style={{color: "#E5E5E5", backgroundColor:"#343434"}}><BsXCircle />  <b>Blocked User</b></ListGroupItem>
      <ListGroupItem style={{color: "#E5E5E5", backgroundColor:"#343434"}}><BsWrench />  <b>Manage Your Review</b></ListGroupItem>
    </ListGroup>
            </Col>
          </Row>
        </TabPane>
      </TabContent>
    </div>

            
   
</body>
</div>
    )
}

export default Settings