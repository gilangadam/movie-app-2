import React, { useState } from "react";
import { Button, Modal } from "reactstrap";
import { FaRegEdit } from "react-icons/fa";
import { FaTrash } from "react-icons/fa";
import { Container, Image, Row, Col } from "react-bootstrap";
import "./assets/styles/profile.scss";
import iwae from "./assets/img/iwae.jpg";
import ModalEditProfile from "./modal-editprofile";

const UserProfile = () => {
  const [modalEditProfile, setModalEditProfile] = useState(false);
  const toggleModalEditProfile = () => setModalEditProfile(!modalEditProfile);

  return (
    <div className="scss-div">
      <Container fluid>
        {/* PROFILE */}
        <Row>
          <Col sm="2" className="profimg">
            <Image src={iwae} width="100px" height="100px" roundedCircle />
            <Button
              className="bt"
              variant="warning"
              size="sm"
              onClick={toggleModalEditProfile}
            >
              <FaRegEdit />
            </Button>{" "}
            <h4>FULLNAME</h4>
            <h6>USERNAME</h6>
          </Col>
          {/* MY REVIEW */}
          <Col sm="10">
            <h1>
              MY REVIEW{" "}
              <Button variant="warning" size="sm">
                <FaRegEdit />
              </Button>{" "}
              <Button variant="warning" size="sm">
                <FaTrash />
              </Button>{" "}
              <Modal isOpen={modalEditProfile} toggle={toggleModalEditProfile}>
                <ModalEditProfile />
              </Modal>
            </h1>
            <div>
              <div>
                <Row>
                  <Col xs="4" className="tes">
                    <Image
                      src={iwae}
                      className="img img-fluid rounded"
                      alt={" thumbnail"}
                    />
                  </Col>
                  <Col xs="8" className="try">
                    <p>
                      <span className="lead font-weight-bolder">Title: </span>
                      <span>title</span>
                    </p>
                    <p>
                      <span className="lead font-weight-bolder">Review: </span>
                      <span>review</span>
                    </p>
                    <p>
                      <span className="lead font-weight-bolder">Score: </span>
                      <span> 10/ 10</span>
                    </p>
                    <p>
                      <span className="lead font-weight-bolder">Rated: </span>
                      <span>rated</span>
                    </p>
                  </Col>
                </Row>
                <hr color="white" />
                <Row>
                  <Col xs="4" className="tes">
                    <Image
                      src={iwae}
                      className="img img-fluid rounded"
                      alt={" thumbnail"}
                    />
                  </Col>
                  <Col xs="8" className="try">
                    <p>
                      <span className="lead font-weight-bolder">Title: </span>
                      <span>title</span>
                    </p>
                    <p>
                      <span className="lead font-weight-bolder">Review: </span>
                      <span>review</span>
                    </p>
                    <p>
                      <span className="lead font-weight-bolder">Score: </span>
                      <span> 10/ 10</span>
                    </p>
                    <p>
                      <span className="lead font-weight-bolder">Rated: </span>
                      <span>rated</span>
                    </p>
                  </Col>
                </Row>
                <hr color="white" />
                <Row>
                  <Col xs="4" className="tes">
                    <Image
                      src={iwae}
                      className="img img-fluid rounded"
                      alt={" thumbnail"}
                    />
                  </Col>
                  <Col xs="8" className="try">
                    <p>
                      <span className="lead font-weight-bolder">Title: </span>
                      <span> title</span>
                    </p>
                    <p>
                      <span className="lead font-weight-bolder">Review: </span>
                      <span>review</span>
                    </p>
                    <p>
                      <span className="lead font-weight-bolder">Score: </span>
                      <span> 10/ 10</span>
                    </p>
                    <p>
                      <span className="lead font-weight-bolder">Rated: </span>
                      <span>rated</span>
                    </p>
                  </Col>
                </Row>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};
export default UserProfile;
