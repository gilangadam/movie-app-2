import React from "react";
import { Component } from "react";
import { Container } from "react-bootstrap";
import "./assets/styles/overview.sass";
class Overview extends Component {
    render() {
        return (
            <div className="sass-dive">
                <Container fluid>
                    <div>
                        <h1 className="synop">Synopsis</h1> <hr className="lines"/>
                        <p className="sy">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.
                        </p>
                        <p className="sy">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.
                        </p>
                        <br/>
                        <h1 className="movieIn">Movie Info</h1> <hr className="line"/>
                        <br/> 
                        <br/> 
                        <p>Release date    : January 5, 1998<br/>
                            Director  : John Doe<br/>
                            Featured song  :  Pegasus fantasi<br/>
                            Budget   : 200 million USD<br/>
                            Release date    : January 5, 1998<br/> 
                            Director  : James Cameron<br/>
                            Featured song  : Soldier dream<br/>
                            Budget   : 200 million USD<br/>
                        </p>
                        <br/>  
                        <p>Release date    : January 5, 1998<br/>
                            Director  : John Doe<br/>
                            Featured song  :  Pegasus fantasi<br/>
                            Budget   : 200 million USD<br/>
                            Release date    : January 5, 1998<br/> 
                            Director  : James Cameron<br/>
                            Featured song  : Soldier dream<br/>
                            Budget   : 200 million USD<br/>
                        </p>
                        <br/>  
                    </div>
                </Container>
            </div>
        )
    }
}
export default Overview;

