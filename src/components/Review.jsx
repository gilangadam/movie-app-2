import React from "react";
import StarRatingComponent from "react-star-rating-component";
import { Button, Container, Image, Row, Col } from "react-bootstrap";
import iwae from "./assets/img/iwae.jpg";
import "./assets/styles/Review.scss";
class Review extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            rateData: {
                rating: 0
            },
            items: []
        };
    }
    onStarClick(nextValue, pervValue, name) {
        this.setState({rating: nextValue});
    }
    componentDidMount(){
        //take random 10 result
        fetch("https://randomuser.me/api/?results=4")
            .then(res => res.json())
            .then(parsedJSON => parsedJSON.results.map(data => (
                {
                    //mapping data sesuai kebutuhan
                    id: `${data.id.name}`,
                    thumbnail: `${data.picture.large}`,
                }
            )))
            .then(items => this.setState({
                items,
                isLoaded: false
            }))
            .catch(error => console.log('parsing failed', error))
    }

    render() {
        const { rating } = this.state;
        const { items } = this.state;
        return(
        <div className="scss-dives">
            <Container fluid>
                <div>
                    <Image src={iwae} 
                    width="100px" 
                    height="100px" 
                    rounded />
                    <h4>USERNAME</h4>
                    <StarRatingComponent
                        className="star"
                        name="rate1"
                        starCount={5}
                        value={rating}
                        emptyStarColor="#E5E5E5"
                        onStarClick={this.onStarClick.bind(this)}
                        />
                        <Container>
                            <input
                            className="textArea"
                            type="textarea"
                            name="review"
                            id="review"
                            placeholder="enter your Review"
                            >
                            </input>
                        </Container>
                    </div>
                    {
                        items.length > 0 ? items.map(item => {
                            const{id, thumbnail} = item
                            return (
                                <div key={id} >
                                    <Container fluid>
                                        <Row>
                                            <Col>
                                                <Image src={thumbnail} 
                                                    width="100px" 
                                                    height="100px" 
                                                    rounded />
                                                <h2 className="uname">USERNAME</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                                            </Col>
                                        </Row>
                                    </Container>
                                </div>
                                
                            )
                        }) : null
                    }
                    <Button variant="warning" size="sm">Load More</Button>
            </Container>

        </div>
    )
    }
}
                        
export default Review;