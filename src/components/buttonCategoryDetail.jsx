import React from "react";
import "./assets/styles/ButtonCategory.sass";


const ButtonCategoryDetail = () => {
  return (
    <div className="content-category-container">
      <div className="content-category-button">
        <button className="button-all">Overview</button>
        <button className="button-anime">Characters</button>
        <button className="button-action">Review</button>
      </div>
    </div>
  );
};

export default ButtonCategoryDetail;
