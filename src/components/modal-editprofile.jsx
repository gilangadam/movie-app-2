import React from "react";
import "./assets/styles/editprofile.scss"
import image from "./assets/img/profilepicture.png"
import { BsXCircle, BsCheck } from "react-icons/bs";

const ModalEditProfile = (props) => {
    
    
  return (
    <div>
      <form>
        <div className="container-edit-profile">
          <header>
            <div className="header-container-edit-profile">
              <button className="close-btn-header-container">
                <BsXCircle />
              </button>
              <div className="header-container-h1">
                <h1>Edit Profile</h1>
              </div>

              <button className="check-btn-header-container">
                <BsCheck />
              </button>
            </div>
            <div className="image">
              <img src={image} className="profile-image" alt=""></img>
            </div>
          </header>

          <label className="label-edit-profile" for="full-name">
            <b>Full Name</b>
          </label>
          <input
            className="input-edit-profile"
            type="text"
            placeholder="edit name"
            name="full-name"
            required
          />

          <label className="label-edit-profile" for="username">
            <b>Username</b>
          </label>
          <input
            className="input-edit-profile"
            type="text"
            placeholder="edit username"
            name="username"
            required
          />

          <label className="label-edit-profile" for="email">
            <b>Email</b>
          </label>
          <input
            className="input-edit-profile"
            type="text"
            placeholder="edit email"
            name="email"
            required
          />

          <label className="label-edit-profile" for="password">
            <b>Password</b>
          </label>
          <input
            className="input-edit-profile"
            type="text"
            placeholder="edit password"
            name="password"
            required
          />

          <label className="label-edit-profile" for="password">
            <b>Confirm Password</b>
          </label>
          <input
            className="input-edit-profile"
            type="text"
            placeholder="confirm password"
            name="password"
            required
          />
          <br></br>

          <button type="submit" className="signupbtn">
            <b>LOGOUT</b>
          </button>
        </div>
      </form>
    </div>
  );
};

export default ModalEditProfile;
