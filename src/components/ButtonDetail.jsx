import React from "react";
import "./assets/styles/DetailContent.scss";
import { Link } from "react-router-dom"

const ButtonDetail = () => {
  return (
    <div>
      <div className="content-category-container">
      <div className="content-category">
      </div>
      <div className="content-category-button">
        <button className="button-overview"><Link className="link" to="/overview">overview</Link></button>
        <button className="button-character"><Link to="/page-character"> character</Link></button>
        <button className="button-review"><Link className="link" to="/review">review</Link></button>
      </div>
      </div>
    </div>
  );
};

export default ButtonDetail;

